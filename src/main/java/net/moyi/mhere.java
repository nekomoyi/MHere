package net.moyi;

import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.Vec3d;
import com.google.gson.Gson;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.registry.CommandRegistry;
import net.minecraft.server.command.CommandManager;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.world.dimension.DimensionType;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Random;

import static com.mojang.brigadier.arguments.StringArgumentType.getString;
import static com.mojang.brigadier.arguments.StringArgumentType.string;

public class mhere implements ModInitializer {
    //private static FabricKeyBinding keyBinding = FabricKeyBinding.Builder.create(new Identifier("MHere","Broadcast"), InputUtil.Type.KEYSYM, GLFW.GLFW_KEY_F9, "MHere keybinds").build();;
    @Override
    public void onInitialize() {
        /*
        KeyBindingRegistry.INSTANCE.register(keyBinding);
        ClientTickCallback.EVENT.register(e -> {
            if(keyBinding.isPressed()){)
        });
         */
        CommandRegistry.INSTANCE.register(false, dispatcher ->{
            dispatcher.register(CommandManager.literal("here")
                .then(CommandManager.literal("help")
                    .executes(ctx -> {
                        Text msg = new LiteralText("§a[Here Help]\n§e");
                        msg.append(new LiteralText("use /here to broadcast your position to all players.\n§e"));
                        msg.append(new LiteralText("use /here [PlayerName] to send to this player.\n"));
                        ctx.getSource().getPlayer().sendMessage(msg);
                        return 0;
                    }))
                .then(CommandManager.argument("name", string())
                    .executes(ctx -> {
                        ServerPlayerEntity player = ctx.getSource().getMinecraftServer().getPlayerManager().getPlayer(getString(ctx, "name"));
                        assert player != null;
                        if(player.getName().getString().equals(getString(ctx, "name"))){
                            Text msg = new LiteralText("§a[Here Private]\n§f");
                            msg.append(this.getHereText(ctx));
                            player.sendMessage(msg);
                            ctx.getSource().getPlayer().sendMessage(new LiteralText("§a[Here] Send your position to " + player.getName().getString()));
                            ctx.getSource().getMinecraftServer().getCommandManager().execute(ctx.getSource().getMinecraftServer().getCommandSource(),"/effect give " + ctx.getSource().getPlayer().getName().getString() + " minecraft:glowing 15");
                        }
                        return 0;
                    }))
                .executes(ctx -> {
                    Text msg = new LiteralText("§a[Here Broadcast]\n§f");
                    msg.append(this.getHereText(ctx));
                    ctx.getSource().getMinecraftServer().getPlayerManager().broadcastChatMessage(msg,false);
                    ctx.getSource().getMinecraftServer().getCommandManager().execute(ctx.getSource().getMinecraftServer().getCommandSource(),"/effect give " + ctx.getSource().getPlayer().getName().getString() + " minecraft:glowing 15");
                    return 0;
                }));
        });
    }
    public String FileToString() {
        String str = "";
        String notexists = new String("[{'start':'我在','end':'。'}]");
        try {
            File file = new File("mhere.json");
            FileInputStream in = new FileInputStream(file);
            if(!file.exists()){
                return notexists;
            }
            int size = in.available();
            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            str = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return str;
    }

    public MHereString[] parseJSONtoObject(){
        String jsonstr = this.FileToString();
        return new Gson().fromJson(jsonstr, MHereString[].class);
    }

    public Text getHereText(CommandContext<ServerCommandSource> ctx) throws CommandSyntaxException {
        Text msg = new LiteralText("");
        Vec3d position = ctx.getSource().getPosition();
        DimensionType dimensionType = ctx.getSource().getPlayer().getEntityWorld().getDimension().getType();
        String dimensionStr = dimensionType.toString();
        if(dimensionType == DimensionType.OVERWORLD){
            dimensionStr = "主世界";
        }else if(dimensionType == DimensionType.THE_NETHER){
            dimensionStr = "地狱";
        }else if(dimensionType == DimensionType.THE_END){
            dimensionStr = "末路之地";
        }
        MHereString[] returnMsg = this.parseJSONtoObject();
        int randnum = new Random().nextInt(returnMsg.length);
        msg.append(new LiteralText(ctx.getSource().getPlayer().getName().getString()));
        msg.append(new LiteralText(":"));
        msg.append(new LiteralText(returnMsg[randnum].getStart()+"§e"));
        msg.append(new LiteralText(dimensionStr));
        msg.append(new LiteralText("("));
        msg.append(new LiteralText(String.format("%.1f", position.getX())));
        msg.append(new LiteralText(", "));
        msg.append(new LiteralText(String.format("%.1f", position.getY())));
        msg.append(new LiteralText(", "));
        msg.append(new LiteralText(String.format("%.1f", position.getZ())));
        msg.append(new LiteralText(")§f"));
        msg.append(new LiteralText(returnMsg[randnum].getEnd()));
        return msg;
    }
}
